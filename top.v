`include "./pll.v"
`include "./uart.v"

// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
	input CLK,    // 16MHz clock
	output LED,   // User/boot LED next to power LED
	output PIN_10,
	output PIN_11,
	output PIN_12,
	output PIN_13,
	output PIN_16,
	output PIN_15,
	output PIN_14,
	output USBPU  // USB pull-up resistor
);
    // drive USB pull-up resistor to '0' to disable USB
	assign USBPU = 0;

	wire rotor1, rotor2, rotor3, rotor4;
	wire uart_rd, uart_tx, uart_rx, bt_con;
	reg [7:0] uart_dat;
	reg [7:0] master_pwm, rotor1_pwm, rotor2_pwm, rotor3_pwm, rotor4_pwm;
	reg [7:0] rotor1_pos_offset, rotor2_pos_offset, rotor3_pos_offset, rotor4_pos_offset;
	reg [7:0] rotor1_neg_offset, rotor2_neg_offset, rotor3_neg_offset, rotor4_neg_offset;
	wire pll_clk, locked;

	uart_rx RX(
		.i_Clock(pll_clk),
		.i_Rx_Serial(uart_rx),
		.o_Rx_DV(uart_rd),
		.o_Rx_Byte(uart_dat)
	);

	pll PLL(
		.clock_in (CLK),
		.clock_out (pll_clk),
		.locked(locked)
	);

	servo_controller ROTOR1(
		.clk(pll_clk),
		.rst(0),
		.position(rotor1_pwm),
		.servo(rotor1)
	);

	servo_controller ROTOR2(
		.clk(pll_clk),
		.rst(0),
		.position(rotor2_pwm),
		.servo(rotor2)
	);

	servo_controller ROTOR3(
		.clk(pll_clk),
		.rst(0),
		.position(rotor3_pwm),
		.servo(rotor3)
	);

	servo_controller ROTOR4(
		.clk(pll_clk),
		.rst(0),
		.position(rotor4_pwm),
		.servo(rotor4)
	);

	initial begin
		rotor1_pwm = 0;
		rotor2_pwm = 0;
		rotor3_pwm = 0;
		rotor4_pwm = 0;
	end

	always @(posedge pll_clk) begin
		rotor1_pwm = master_pwm - rotor1_neg_offset;
		rotor2_pwm = master_pwm - rotor2_neg_offset;
		rotor3_pwm = master_pwm - rotor3_neg_offset;
		rotor4_pwm = master_pwm - rotor4_neg_offset;

		if ( uart_rd == 1 ) begin
			case (uart_dat)
				0: master_pwm <= 0;
				1: master_pwm <= 28;
				2: master_pwm <= 56;
				3: master_pwm <= 85;
				4: master_pwm <= 114;
				5: master_pwm <= 142;
				6: master_pwm <= 170;
				7: master_pwm <= 200;
				8: master_pwm <= 226;
				9: master_pwm <= 255;
				q: master_pwm <= 255;
				F: begin
					rotor1_neg_offset = 10;
					rotor2_neg_offset = 10;
					rotor3_neg_offset = 0;
					rotor4_neg_offset = 0;
				end
				B: begin
					rotor1_neg_offset = 0;
					rotor2_neg_offset = 0;
					rotor3_neg_offset = 10;
					rotor4_neg_offset = 10;
				end
				L: begin
					rotor1_neg_offset = 10;
					rotor2_neg_offset = 0;
					rotor3_neg_offset = 0;
					rotor4_neg_offset = 10;
				end
				R: begin
					rotor1_neg_offset = 0;
					rotor2_neg_offset = 10;
					rotor3_neg_offset = 10;
					rotor4_neg_offset = 0;
				end
				G: begin
					rotor1_neg_offset = 10;
					rotor2_neg_offset = 0;
					rotor3_neg_offset = 0;
					rotor4_neg_offset = 0;
				end
				I: begin
					rotor1_neg_offset = 0;
					rotor2_neg_offset = 10;
					rotor3_neg_offset = 0;
					rotor4_neg_offset = 0;
				end
				H: begin
					rotor1_neg_offset = 0;
					rotor2_neg_offset = 0;
					rotor3_neg_offset = 0;
					rotor4_neg_offset = 10;
				end
				J: begin
					rotor1_neg_offset = 0;
					rotor2_neg_offset = 0;
					rotor3_neg_offset = 10;
					rotor4_neg_offset = 0;
				end
				S: begin
					rotor1_neg_offset = 0;
					rotor2_neg_offset = 0;
					rotor3_neg_offset = 0;
					rotor4_neg_offset = 0;
				end
			endcase
		end
		
	end

    assign PIN_10 = rotor1;
    assign PIN_11 = rotor2;
    assign PIN_12 = rotor3;
    assign PIN_13 = rotor4;
	assign uart_rx = PIN_16;
	assign PIN_14 = bt_con;

endmodule

module servo_controller (
  input clk,
  input rst,
  input [7:0] position,
  output servo
  );
	
  reg pwm_q, pwm_d;
  reg [19:0] ctr_q, ctr_d;
  assign servo = pwm_q;
  //position (0-255) maps to 50,000-100,000 (which corresponds to 1ms-2ms @ 50MHz)
  //this is approximately (position+165)<<8
  //The servo output is set by comparing the position input with the value of the counter (ctr_q)
  always @(*) begin
    ctr_d = ctr_q + 1'b1;
    if (position + 9'd165 > ctr_q[19:8]) begin
      pwm_d = 1'b1;
    end else begin
      pwm_d = 1'b0;
    end
  end
	
  always @(posedge clk) begin
    if (rst) begin
      ctr_q <= 1'b0;
    end else begin
      ctr_q <= ctr_d;
    end
    pwm_q <= pwm_d;
  end
endmodule
